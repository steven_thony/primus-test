<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Group extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ms_group';
    public $primaryKey='group_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['group_name','group_users'];

    public function users()
    {
        return $this->hasOne('App\Models\Users');
    }

}
