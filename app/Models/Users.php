<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Users extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ms_user';
    public $primaryKey='user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['group_id','user_name', 'user_gender','user_photo','date','total_group'];

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

}
