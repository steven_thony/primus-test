<?php 
namespace App\Http\Controllers;

use View;
use DB;
use Input;
use Session;
use Mail;

use App\Models\Users;
use App\Models\Group;

use App\Http\Requests;
use Illuminate\Http\Request;

class UserController extends Controller 
{ 
	//admin
	public function view()
	{
		$user = Users::all();
		$count_user = count($user);
		return View('user.admin',compact('user','count_user'));
	}

	public function load_addUser()
	{
		/*		
		=======================================
		Usage : show add user
		Table : msuser
		=======================================
		*/

		$form_method_flag = 'add';
		$group = Group::all();
		return View('user.userform',compact('form_method_flag','group'));
	}


	public function user_create(Request $request)
	{
		/*		
		=======================================
		Usage : add user
		Table : msuser
		=======================================
		*/

		$user = new Users;
		$user->user_name = Input::get('name');
		$user->user_gender = Input::get('gender');
		$user->group_id = json_encode(Input::get('checkbox_id'));
		$user->date = Input::get('date');
		$user->total_group = count(Input::get('checkbox_id'));
 		$user->save();

		$files = $request->file('images');
    
	    $filename = 'user'.date('his').$files->getClientOriginalName();
	    $destinationPath = public_path().'/img/user';
	    $files->move($destinationPath, $filename);
	    $user->user_photo = $filename;
	    $user->save();
		
		$user = Users::all();
		$count_user = count($user);

		return View('user.admin',compact('user','count_user'));
	}

	public function populate_edituser($id)
	{
		/*		
		=======================================
		Usage : show edit user
		Table : msuser
		=======================================
		*/

		$user = Users::where('user_id',$id)->first();
		$checkbox = json_decode($user->group_id);
		$group = Group::all();
		$form_method_flag = 'edit';

		return View('user.userform',compact('user','form_method_flag','group','checkbox'));
	}

	public function user_edits($id)
	{
		/*		
		=======================================
		Usage : edit user
		Table : msuser
		=======================================
		*/
		$user = Users::where('user_id',$id)->first();
		$user->user_name = Input::get('name');
		$user->user_gender = Input::get('gender');
		$user->group_id = json_encode(Input::get('checkbox_id'));
		$user->date = Input::get('date');
		$user->total_group = count(Input::get('checkbox_id'));
 		$user->save();

 		$files = $request->file('images');
    
	    $filename = 'user'.date('his').$file->getClientOriginalName();
	    $destinationPath = public_path().'/img/user';
	    $file->move($destinationPath, $filename);
	    $user->user_photo = $filename;
	    $user->save();
		
		$user = Users::all();
		$count_user = count($user);

		return View('user.admin',compact('user','count_user'));
	}


	public function user_delete($arrayid)
	{
		/*		
		=======================================
		Usage : delete user
		Table : msuser
		=======================================
		*/
		$arr = explode(",",$arrayid);
		DB::table('ms_user')->whereIn('user_id', $arr)->delete();

		$user = User::all();
		$count_user = count($user);
		return View('user.admin',compact('user','count_user'));
	}

	public function user_details($id)
	{
		$user = Users::where('user_id',$id)->first();
		return View('user.user-details',compact('user'));
	}

	public function user_detail_group($getid)
	{
		$user = Users::where('group_id','LIKE',"%{$getid}%")->get();
		return view('group.user',compact('user'));
	}

	
}