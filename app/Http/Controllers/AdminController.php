<?php
namespace App\Http\Controllers;
use Input;
use Auth;
use Redirect;
use DB;
use Session;

use App\Models\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\View;

use Illuminate\Support\Facades\Validator;
use Illuminate\Cookies\CookieServiceProvider;
use Illuminate\Session\Middleware\StartSession;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\CreateStandardCrudRequest;
use App\Http\Requests\UploadImageRequest;
use Carbon\Carbon;

use form_method_flag;


class AdminController extends Controller {


	//get login page
	public function login()
	{
		return view('admin.login');
	}

	//post login data
	public function doLogin(Request $request)
	{
		
		// create our user data for the authentication
		$userdata = array(
		'email'       => Input::get('email'),
		'password'  => Input::get('password')
		);

		// attempt to do the login
		if (Auth::attempt($userdata))
		{

		// validation successful!
		// redirect them to the secure section or whatever
		// return Redirect::to('secure');

		return view('admin.main');
		}
		else
		{
		Session::flash('error','Wrong email or password!');
		return view('admin.login');
		// validation not successful, send back to form
		//return Redirect::to('login');
		}
	}

	public function logout()
	{
		Auth::logout();
		return view('admin.login');
	}


}