<?php 
namespace App\Http\Controllers;

use View;
use DB;
use Input;
use Session;
use Mail;

use App\Models\Group;

use App\Http\Requests;
use Illuminate\Http\Request;

class GroupController extends Controller 
{ 
	//admin
	public function view()
	{
		$group = Group::all();
		$count_group = count($group);
		return View('group.admin',compact('group','count_group'));
	}

	public function load_addUser()
	{
		/*		
		=======================================
		Usage : show add group
		Table : msgroup
		=======================================
		*/

		$form_method_flag = 'add';
		return View('group.groupform',compact('form_method_flag'));
	}


	public function group_create(Request $request)
	{
		/*		
		=======================================
		Usage : add group
		Table : msgroup
		=======================================
		*/


		$group = new Group;
		$group->group_name = Input::get('name');
 		$group->save();
		
		$group = Group::all();
		$count_group = count($group);

		return View('group.admin',compact('group','count_group'));
	}

	public function populate_editgroup($id)
	{
		/*		
		=======================================
		Usage : show edit group
		Table : msgroup
		=======================================
		*/

		$group = Group::where('group_id',$id)->first();
		$form_method_flag = 'edit';

		return View('group.groupform',compact('group','form_method_flag'));
	}

	public function group_edits($id)
	{
		/*		
		=======================================
		Usage : edit group
		Table : msgroup
		=======================================
		*/
		$group = Group::where('group_id',$id)->first();
		$group->group_name = Input::get('name');
 		$group->save();
		
		$group = Group::all();
		$count_group = count($group);

		return View('group.admin',compact('group','count_group'));
	}


	public function group_delete($arrayid)
	{
		/*		
		=======================================
		Usage : delete group
		Table : msgroup
		=======================================
		*/
		$arr = explode(",",$arrayid);
		DB::table('ms_group')->whereIn('group_id', $arr)->delete();

		$group = Group::all();
		$count_group = count($group);
		return View('group.admin',compact('group','count_group'));
	}

	
}