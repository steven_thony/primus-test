<div class="ajaxify-wrapper">
<div class="box">
      <div class="box-header">

        <h3 class="box-title">{{$count_group}} Group</h3>
      </div><!-- /.box-header -->

      <button class="add-btn btn btn-success btn-lg" style="padding:8px 25px;"><i class="glyphicon glyphicon-plus-sign"  aria-hidden="true"></i><br/><span style="font-size:0.7em;"> Add</span></button>

      <button  class="btn btn-danger btn-lg" data-toggle="modal" data-target="#delete-modal" style="padding:8px 20px;"><i class="glyphicon glyphicon-trash"  aria-hidden="true"></i><br/><span style="font-size:0.7em;">Delete</span></button>

      <div class="box-body">
      
      

    <!--<img class="loading_main" src="{{ asset('assets/fix/bar120.gif') }}">-->
      <table data-source="#" data-filter="#filter_table" id="standardcrud_datatable" class="table table-bordered table-striped">

        <thead>
          <tr>  
              <th> Action </th>
              <th> Id   </th>
              <th> Name </th>
              <th> Date </th>
              <th> Total User </th>
              <th> User Details </th>
          </tr> 
        </thead>
        <tbody align="center">
        @foreach($group as $group)
        <?php $user = \App\Models\Users::where('group_id','LIKE',"%{$group->group_id}%")->count(); ?>
              <tr>
                  <td>
                  <input id="checkbox" type="checkbox" name="checkbox_id[]" value="{{ $group->group_id }}">
                  <button class="btn btn-default edit-btn" id="edit{{ $group->group_id }}" data-toggle="modal" data-target="#edit-modal">
                  <i class="glyphicon glyphicon-edit"></i>
                  </button>
                  </td>
                  <td>{{$group->group_id}}</td>
                  <td>{{$group->group_name}}</td>
                  <td>{{  date("D, d M Y",strtotime($group->created_at))}}</td>
                  <td>{{$user}}</td>
                  <td> <button class="btn btn-default user-btn" id="{{ $group->group_id }}" data-toggle="modal" data-target="#user-modal"><i class="fa fa-eye"></i></button> </td>
              </tr>
        @endforeach
        </tbody>
      </table>

    </div>
</div>


  <!-- DATA TABLES SCRIPT -->

  <!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Add New Group</h4>
              </div>

              <iframe name="upload-frame" id="upload-frame" style="display:none;"></iframe>
              <div id="addform-content">
                <!-- load form via Ajax -->
                <form action="{{ url('group_create') }}" id="createForm" method="post" target="upload-frame" enctype="multipart/form-data">
              </div> <!-- /#addform-content -->
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- EDIT MODAL -->
  <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Edit Group</h4>
              </div>

              <iframe name="upload-frame-2" id="upload-frame-2" style="display:none;"></iframe>
              <div id="editform-content">

                <!-- load form via Ajax -->
               
              </div> <!-- /#editform-content -->
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


  <!-- DELETE MODAL -->
    <div class="modal fade" id="delete-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">DELETE</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure to delete these records? &hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
            <button type="button" class="delete-btn btn btn-primary">Delete</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <!-- User Detail MODAL -->
  <div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> User List</h4>
              </div>

              <iframe name="upload-frame-2" id="upload-frame-2" style="display:none;"></iframe>
              <div id="userform-content">

                <!-- load form via Ajax -->
               
              </div> <!-- /#editform-content -->
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>



<script type="text/javascript">
$(".loading_main").hide();
$('head').append('<link href="{{ URL::asset('public/assets/lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />');

    $("#standardcrud_datatable").dataTable();
    // load Add Modal Form
    $(".add-btn").on('click',function(){

      $('#compose-modal').modal({ show: true,backdrop: false });
      $.ajax({
        url: '{{ URL('load_addGroup') }}',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $("#addform-content").html(message);
          $('#compose-modal').modal({backdrop: false,hide:true});
        }
        
      });
    });


    //populate Edit Modal Form
    $("#standardcrud_datatable").on('click','.edit-btn',function(){
      var getid = $(this).attr('id');
      var url = getid.replace(/^edit+/, "");
      $.ajax({
        url: '{{ URL('populate_editgroup') }}/'+url+"/edit",
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $("#editform-content").html(message);
        }
      });
    });

    //populate User Modal Form
    $("#standardcrud_datatable").on('click','.user-btn',function(){
      var getid = $(this).attr('id');
      $.ajax({
        url: '{{ URL('populate_usergroup') }}/'+getid,
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $("#userform-content").html(message);
        }
      });
    });

    $(".modal").on('click','.submit-btn1',function(){

      $('#compose-modal').modal('hide');
      $('#edit-modal').modal('hide');

      $.ajax({
        url: '{{ URL('GroupList') }}',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          //$(".conten t").remove();
          $(".content").html(message);
        },
        complete:function(){
        $.ajax({
        url: '{{ URL('GroupList') }}',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $(".content").html(message);
          }
      });
        }
      });

    });


    //validasi
    $(".modal").on('click','.submit-btn',function(){
      var valid = 1;
      var count= 0;
      var error=[];
      var text='';

      if($('#input-name').val() == '')
      {
        document.getElementById('input-name').style.borderColor = "red";
        error[count] = 'Name must be filled!';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-name').style.borderColor = "green"; 
      }


      for (i = 0; i < count; i++) {
       text += error[i] + "\n";
      }
      
      if(valid == 0){
        alert(text);
        return false;
      }

      $('#compose-modal').modal('hide');
      $('#edit-modal').modal('hide');

      $.ajax({
        url: '{{ URL('GroupList') }}',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          //$(".content").remove();
          $(".content").html(message);
        },
        complete:function(){
           $.ajax({

            url: '{{ URL('GroupList') }}',
            beforeSend: function () {

              $(".loading_main").show();
            },
            success: function (message) {
                //alert(message);
              $(".loading_main").hide();
                //$(".conten t").remove();
              $(".content").html(message);
            }
          });
         }
      });
    });

$(".btn-danger").click(function(){
  var valid=1;
  if($('input#checkbox').is(':checked')){
  
            valid;
          }
        else{
            alert("You didn't check it!");
            valid = 0;
    }

    if(valid == 0){
        return false;
      }
});
  
    
    $(".delete-btn").on('click',function () {
      $('#delete-modal').modal('hide');

        var searchIDs = $('input[name="checkbox_id[]"]:checked').map(function(){
          return $(this).val();
        }).get();

        $.ajax({
          url: '{{ URL('group_delete') }}/'+searchIDs,
          beforeSend: function () {
            $(".loading_main").show();
          },
          success: function (message) {
            //alert(message);
            $(".loading_main").hide();
            $(".content").html(message);
          },
          complete:function()
          {
          $.ajax({
        url: '{{ URL('GroupList') }}',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $(".content").html(message);
        }
      });
          }
        });
    });
</script>