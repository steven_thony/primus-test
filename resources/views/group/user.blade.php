 <div class="modal-body">

 @foreach($user as $user)
 <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">ID</span>
        <input id="text" type="text" name="id" value="{{ $user->user_id }}" >
    </div>
  </div>

  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Name</span>
        <input id="name" type="text" name="name" value="{{ $user->user_name }}" >
    </div>
  </div>

  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Gender</span>
        <input id="gender" type="text" name="gender" value="{{ $user->user_gender }}" >
    </div>
  </div>
  <br/><br/>
  @endforeach

 </div>