<script>
function readURL(input) {
  if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function (e) {
              $('#image').attr('src', e.target.result);
          }
          
          reader.readAsDataURL(input.files[0]);
      }
  }

$("#input-photo").change(function(){
      readURL(this);
  });

</script>
<!-- use this for create -->
@if($form_method_flag=='add')
<form action="{{ url('user_create') }}" id="createForm" method="post" target="upload-frame" enctype="multipart/form-data">
@endif
<!-- use this for update -->
@if($form_method_flag=='edit')
<form action="{{ url('user_edits') }}/{{ $user->user_id }}/edit" id="editForm" method="post" target="upload-frame-2" enctype="multipart/form-data">
@endif
    <!-- only use this for update -->

    @if($form_method_flag=='edit')
    <input type="hidden" name="_method" value="PUT">
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal-body">

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Group</span>
        @foreach($group as $group)
        <span>{{$group->group_name}}</span>
        <input id="checkbox" type="checkbox" name="checkbox_id[]" value="{{ $group->group_id }}" <?php if(isset($user)){ if(strpos($user->group_id,strval($group->group_id) ) !== FALSE ) echo 'checked'; } ?>>
        @endforeach
    </div>
  </div>
        
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">Name</span>
                <input name="name" id="input-name" type="text" class="form-control" value="<?php if(isset($user)) echo $user->user_name;?>">
            </div>
        </div>


        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">Gender</span>
                <select id="input-gender" class="form-control" name="gender">
                    <option value="">-- Select --</option>
                    <option value="0" <?php if(isset($user) && $user->user_gender == 0) echo "selected"  ?> >Female</option>
                    <option value="1" <?php if(isset($user) && $user->user_gender == 1) echo "selected"  ?>>Male</option>
                </select>
            </div>
        </div>


        <div class="form-group">
        <div class="btn btn-success btn-file">
            <i class="fa fa-paperclip"></i> Photo
            <input type="file" id="input-photo" name="images" accept="image/jpg,image/png" multiple/>
        </div>
            <img id="image" src="{{asset('public/img/user')}}/<?php if(isset($user->user_photo)) echo  $user->user_photo;else echo '#'; ?>" alt="" width="150px" height="150px" />
        </div>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">Date</span>
                <input name="date" id="input-date" type="text" class="form-control date" value="<?php if(isset($user)) echo $user->date;?>">
            </div>
        </div>

    </div>

    <div class="modal-footer clearfix">

        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>

        @if($form_method_flag=='add')
        <button type="submit" class="submit-btn btn btn-primary pull-left"><i class="fa fa-envelope"></i> Add</button>
        @endif
        @if($form_method_flag=='edit')
        <button type="submit" class="submit-btn1 btn btn-primary pull-left"><i class="fa fa-envelope"></i> Update</button>
        @endif
    </div>
</form>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript">
    $('.date').datepicker();
</script>