<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
	.profile-img{
	height: 250px;
	width: 250px;
	}
	img{
		height: 100%;
		width: 100%;
		border: 1px solid #ccc;
		border-radius: 50%;
	}

</style>
<div class="container">
	<div class="box-content">
		<div class="row">
<div class="row">
				<div class="col-sm-12">
					<div class="col-md-8 col-sm-12 form-half">

					<div class="row">
			          		<div class="form-group">
								<label class="col-xs-4">User ID</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" value="{{$user->user_id}}">
								</div>
							</div>
						</div>

						<div class="row">
			          		<div class="form-group">
								<label class="col-xs-4">User Fullname</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" value="{{$user->user_name}}">
								</div>
							</div>
						</div>
						
						<div class="row">
			          		<div class="form-group">
								<label class="col-xs-4">User Gender</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" value="{{$user->user_gender ? 'Male' : 'Female'}}">
								</div>
							</div>
						</div>
						<div class="row">
			          		<div class="form-group">
								<label class="col-xs-4">Join Date</label>
								<div class="col-xs-8">
									<input type="text" class="form-control"  value="{{ date('D, d M Y',strtotime($user->date))}}">
								</div>
							</div>
						</div>
						<div class="row">
			          		<div class="form-group">
								<label class="col-xs-4 ">Group</label>
								<div class="col-xs-8">
									<input type="text" class="form-control"  value="{{$user->total_group}}">
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-4 col-sm-4">
						<div class="row">
							<div class="col-md-12">
								<div class="profile-img">
								<img src="{{asset('/public/img')}}/user/{{$user->user_photo}}" onError="this.onerror=null;this.src='{{ asset('/public/img/default.jpg') }}';" class="img-responsive">
								</div>
							</div>
						</div>
						<br>
					</div>
				</div>
	</div>
	</div>
</div>
</div>