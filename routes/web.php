<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//get login page
Route::get('/','AdminController@login');
//login post
Route::post('/login','AdminController@doLogin');
//logout
Route::get('/logout','AdminController@logout');

#user
//user detail
Route::get('user-details/{id}','UserController@user_details');
//view 
Route::get('UserList','UserController@view');
//add
Route::get('load_addUser', 'UserController@load_addUser');
//create
Route::post('user_create', 'UserController@user_create');
//edit
Route::get('populate_edituser/{id}/edit','UserController@populate_edituser');
//update
Route::put('user_edits/{id}/edit', array('uses' => 'UserController@user_edits'));
//delete
Route::get('user_delete/{arrayid}', array('uses' => 'UserController@user_delete'));
//user detail
Route::get('populate_usergroup/{getid}','UserController@user_detail_group');

#group
//view 
Route::get('GroupList','GroupController@view');
//add
Route::get('load_addGroup', 'GroupController@load_addGroup');
//create
Route::post('group_create', 'GroupController@group_create');
//edit
Route::get('populate_editgroup/{id}/edit','GroupController@populate_editgroup');
//update
Route::put('group_edits/{id}/edit', array('uses' => 'GroupController@group_edits'));
//delete
Route::get('group_delete/{arrayid}', array('uses' => 'GroupController@group_delete'));

